// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: ' AIzaSyAg2jgE4-HaU5i1KhNXpR56TAHbu8yfJVI',
    databaseURL : ' https://hacker-news-app-dc72a.firebaseio.com',
    projectId: 'hacker-news-app-dc72a',
    storageBucket: 'hacker-news-app-dc72a.appspot.com',
    messagingSenderId: '234217486535'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
