import {Injectable} from '@angular/core';
import {TraccarService} from '../traccar/traccar.service';
import {Storage} from '@ionic/storage';
import {fromPromise} from 'rxjs/internal-compatibility';
import {userKey} from '../../shared/consts';
import {map, mergeMap, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {User} from '../../model';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(private traccarService: TraccarService, private storage: Storage, private firebase: FirebaseX) {
    }

    public registerToken(token: string): Observable<boolean> {
        return fromPromise(this.storage.get(userKey)).pipe(
            mergeMap((user: User) => {
                if (!user) {
                    return of(false);
                } else {
                    const clonedUser: User = {...user, attributes: {...user.attributes}};
                    if (this.notificationTokensNotDefined(clonedUser) || this.tokenNotRegisteredYet(clonedUser, token)) {
                        if (this.notificationTokensNotDefined(clonedUser)) {
                            clonedUser.attributes.notificationTokens = token;
                        } else {
                            clonedUser.attributes.notificationTokens += ',' + token;
                        }
                        return this.traccarService.oneUserPut(clonedUser).pipe(
                            tap(x => {
                                if (x !== undefined) {
                                    x.password = user.password;
                                    this.storage.set(userKey, x);
                                }
                            }),
                            map(value => value !== undefined)
                        );
                    }
                }
            })
        );
    }

    public unregisterToken(): Promise<any> {
        return this.firebase.unregister();
    }

    private notificationTokensNotDefined(user: User): boolean {
        return !user.attributes.notificationTokens;
    }

    private tokenNotRegisteredYet(user: User, token: string): boolean {
        return user.attributes.notificationTokens.indexOf(token) < 0;
    }
}
