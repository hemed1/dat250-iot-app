import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import {langKey} from '../../shared/consts';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  selected = '';
  private defaultAppLanguage = 'en';

  static getLanguages() {
    return [
      { text: 'English', value: 'en', img: 'assets/images/en.jpg' },
      { text: 'عربي', value: 'ar', img: 'assets/images/ar-oman.svg'}
    ];
  }


  private static switchDirection(lang: string): void {
    if (lang === 'ar') {
      document.body.setAttribute('dir', 'rtl');
    } else {// otherwise use default
      document.body.setAttribute('dir', 'ltr');
    }
  }

  constructor(private translate: TranslateService, private storage: Storage) {
  }

  setInitialAppLanguage() {
    this.translate.setDefaultLang(this.defaultAppLanguage);
    this.storage.get(langKey).then(val => {
      if (val) {
        this.setLanguage(val);
        this.selected = val;
      }
    });
  }


  setLanguage(lng) {
    this.translate.use(lng);
    this.selected = lng;
    this.storage.set(langKey, lng);
    LanguageService.switchDirection(lng);
  }
}
