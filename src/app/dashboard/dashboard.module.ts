import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DashboardPage} from './dashboard-page.component';
import {TranslateModule} from '@ngx-translate/core';
import {HighchartsChartModule} from 'highcharts-angular';
import { GoogleChartsModule } from 'angular-google-charts';



@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        HighchartsChartModule,
        GoogleChartsModule,
        RouterModule.forChild([{path: '', component: DashboardPage}]),
        TranslateModule.forChild()
    ],
    declarations: [DashboardPage]
})
export class DashboardPageModule {
}
