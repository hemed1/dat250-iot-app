import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SettingsPage} from './settings-page.component';

const routes: Routes = [
    {
        path: '',
        component: SettingsPage
    },
    {
        path: 'account',
        loadChildren: () => import('../pages/account/account.module').then(m => m.AccountPageModule)
    },  { path: 'language', loadChildren: './language/language.module#LanguagePageModule' }

];
@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})
export class SettingsRoutingModule {
}
