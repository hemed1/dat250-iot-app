import { PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { LanguageService } from './../../services/language/language.service';

@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss']
})
export class LanguagePage implements OnInit {
  languages = [];
  selected = '';

  constructor(
    private popOver: PopoverController,
    private langService: LanguageService
  ) {}

  ngOnInit() {
    this.languages = LanguageService.getLanguages();
    this.selected = this.langService.selected;
  }

  select(lng) {
    this.langService.setLanguage(lng);
    this.popOver.dismiss();
  }
}
