import { LanguagePage } from './language/language.page';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from '../model';
import { userKey } from '../shared/consts';

import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings-page.component.html',
  styleUrls: ['settings-page.component.scss']
})
export class SettingsPage {
  user: User;

  constructor(
    private storage: Storage,
    private popOverCtrl: PopoverController
  ) {}
  ionViewDidEnter() {
    this.getUserFromStorage();
  }
  private getUserFromStorage() {
    this.storage.get(userKey).then((user: User) => {
      this.user = user;
    });
  }

  async openLanguage(ev: any) {
    const popover = await this.popOverCtrl.create({
      component: LanguagePage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
}
