import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SettingsPage} from './settings-page.component';
import {TranslateModule} from '@ngx-translate/core';
import {SettingsRoutingModule} from './settings-routing.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        TranslateModule.forChild(),
        SettingsRoutingModule
    ],
    declarations: [SettingsPage]
})
export class SettingsPageModule {
}
