import {Hive} from './hive.model';

export interface Position {
    id: number;
    deviceId?: number;
    device?: Hive;
    attributes: PositionAttributes;
    latitude: number;
    longitude: number;
    deviceTime?: Date;
}

interface PositionAttributes {
    temperature: number;
    humidity: number;
    batteryLevel: number;
    motion?: boolean;
}
