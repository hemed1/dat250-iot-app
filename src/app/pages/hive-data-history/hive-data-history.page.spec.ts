import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiveDataHistoryPage } from './hive-data-history.page';

describe('HiveDataHistoryPage', () => {
  let component: HiveDataHistoryPage;
  let fixture: ComponentFixture<HiveDataHistoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiveDataHistoryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiveDataHistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
