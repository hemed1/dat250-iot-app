import {Component} from '@angular/core';
import {LanguagePage} from '../../settings/language/language.page';
import {PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage {

    constructor(
        private popOverCtrl: PopoverController
    ) {}

    async openLanguage(ev: any) {
        const popover = await this.popOverCtrl.create({
            component: LanguagePage,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }

}
