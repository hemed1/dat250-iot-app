import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {User} from '../../model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoadingController, ToastController} from '@ionic/angular';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

    private unsubscriber$ = new Subject();
    private loginErrorString: string;
    private loginLoadingMessage: string;
    user: User = {
        email: '',
        password: ''
    };

    constructor(private authenticationService: AuthenticationService,
                private router: Router, private loadingController: LoadingController,
                private translateService: TranslateService, private toastController: ToastController) {
    }

    ngOnInit() {
        this.translateService.get(['LOGIN_ERROR', 'LOGIN_LOADING_MESSAGE']).pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(values => {
            this.loginErrorString = values.LOGIN_ERROR;
            this.loginLoadingMessage = values.LOGIN_LOADING_MESSAGE;
        });
    }

    login() {
        this.showLoader().then(loading => {
            loading.present();
            this.authenticationService.authenticate(this.user).pipe(
                takeUntil(this.unsubscriber$)
            ).subscribe(responseStatusCode => {
                if (responseStatusCode === 200) {
                    loading.dismiss().then(() => {
                        this.router.navigateByUrl('tabs/dashboard');
                    });
                } else {
                    this.loadingController.dismiss().then(() => {
                        loading.dismiss().then(() => {
                            this.showToast();
                        });
                    });
                }
            });
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private showLoader() {
        return this.loadingController.create({
            spinner: null,
            message: this.loginLoadingMessage,
            translucent: true,
            cssClass: 'custom-class-to-customize-spinner',
            animated: true,
            backdropDismiss: true
        });
    }
    private showToast() {
        this.toastController.create({
            message: this.loginErrorString,
            duration: 3000,
            position: 'top',
            buttons: ['Ok'],
            cssClass: 'custom-class-to-customize-toast'
        }).then(toast => {
            toast.present();
        });
    }
}
