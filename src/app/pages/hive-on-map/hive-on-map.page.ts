import { Component, OnInit } from '@angular/core';
import { Hive, Position } from '../../model';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import {
  Environment,
  Geocoder,
  GeocoderRequest,
  GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  HtmlInfoWindow,
  Marker,
  GeocoderResult,
  ILatLng
} from '@ionic-native/google-maps/ngx';

@Component({
  selector: 'app-hive-on-map',
  templateUrl: './hive-on-map.page.html',
  styleUrls: ['./hive-on-map.page.scss']
})
export class HiveOnMapPage implements OnInit {
  currentPosition: Position;
  hive: Hive = { name: '', uniqueId: '', attributes: {} };
  map: GoogleMap;
  geocoder: Geocoder;
  infoWindow: HtmlInfoWindow;
  loading: any;

  constructor(
    private router: Router,
    private platform: Platform,
    private loadCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.hive = this.router.getCurrentNavigation().extras.state.hive;
    this.currentPosition = this.router.getCurrentNavigation().extras.state.currentPosition;
    this.loadCtrl.create().then(load => {
      load.present();
      this.platform.ready();
      this.loadMap();
      load.dismiss();
    });
  }

  getMarkerTitle() {
    const geoRequest: GeocoderRequest = {
      position: [
        {
          lat: this.currentPosition.latitude,
          lng: this.currentPosition.longitude
        }
      ]
    };
    this.geocoder.geocode(geoRequest).then(results => {
      console.log('Geo decode: ' + results + ' ' + results[0]);
    });
  }

  loadMap1() {
    // This code is necessary for browser
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyBFO_-35lGKcrLO4CVe31bsJ9dSqb9O7-A',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyBFO_-35lGKcrLO4CVe31bsJ9dSqb9O7-A'
    });
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: this.currentPosition.latitude,
          lng: this.currentPosition.longitude
        },
        zoom: 13,
        tilt: 30
      }
    });
    const marker: Marker = this.map.addMarkerSync({
      title: this.hive.name,
      icon: 'red',
      animation: 'DROP',
      position: {
        lat: this.currentPosition.latitude,
        lng: this.currentPosition.longitude
      }
    });
    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
      this.getMarkerTitle(); // test marker title
    });
  }

  loadMap() {
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyBFO_-35lGKcrLO4CVe31bsJ9dSqb9O7-A',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyBFO_-35lGKcrLO4CVe31bsJ9dSqb9O7-A'
    });
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: this.currentPosition.latitude,
          lng: this.currentPosition.longitude
        },
        zoom: 14,
        tilt: 30
      }
    });

    const marker: Marker = this.map.addMarkerSync({
      title: this.hive.name,
      icon: 'red',
      animation: 'DROP',
      position: {
        lat: this.currentPosition.latitude,
        lng: this.currentPosition.longitude
      }
    });

    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //   this.getMarkerTitle(); // test marker title
    // });

    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((params: any[]) => {
      const latLng: ILatLng = params[0];
      // tslint:disable-next-line: no-shadowed-variable
      const marker: Marker = this.map.addMarkerSync({
        position: latLng
      });

      // Latitude, longitude -> address
      Geocoder.geocode({
        position: latLng
      }).then((results: GeocoderResult[]) => {
        // tslint:disable-next-line: triple-equals
        if (results.length == 0) {
          // Not found
          return null;
        }
        const address: any = [
          results[0].subThoroughfare || '',
          results[0].thoroughfare || '',
          results[0].locality || '',
          results[0].adminArea || '',
          results[0].postalCode || '',
          results[0].country || ''
        ].join(', ');

        marker.setTitle(address);
        marker.showInfoWindow();
      });
    });
  }
}
